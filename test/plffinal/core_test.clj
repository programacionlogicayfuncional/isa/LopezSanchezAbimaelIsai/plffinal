(ns plffinal.core-test
  (:require [clojure.test :refer :all]
            [plffinal.problema1 :as problema1]
            [plffinal.problema2 :as problema2]
            [plffinal.problema3 :as problema3]
            [plffinal.problema4 :as problema4]))

(deftest regresa-al-punto-de-origen?-test
  (testing "Pruebas unitarias problema 1"
    (is (true? (problema1/regresa-al-punto-de-origen? "")))
    (is (true? (problema1/regresa-al-punto-de-origen? [])))
    (is (true? (problema1/regresa-al-punto-de-origen? (list))))
    (is (true? (problema1/regresa-al-punto-de-origen? (list \> \<))))
    (is (true? (problema1/regresa-al-punto-de-origen? "v^")))
    (is (true? (problema1/regresa-al-punto-de-origen? [\v \^])))
    (is (true? (problema1/regresa-al-punto-de-origen? "^>v<")))
    (is (true? (problema1/regresa-al-punto-de-origen? (list \^ \> \v \<))))
    (is (true? (problema1/regresa-al-punto-de-origen? "<<vv>>^^")))
    (is (true? (problema1/regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])))

    (is (false? (problema1/regresa-al-punto-de-origen? ">")))
    (is (false? (problema1/regresa-al-punto-de-origen? (list \>))))
    (is (false? (problema1/regresa-al-punto-de-origen? "<^")))
    (is (false? (problema1/regresa-al-punto-de-origen? [\< \^])))
    (is (false? (problema1/regresa-al-punto-de-origen? ">>><<")))
    (is (false? (problema1/regresa-al-punto-de-origen? (list \> \> \> \< \<))))
    (is (false? (problema1/regresa-al-punto-de-origen? [\v \v \^ \^ \^])))))

(deftest regresan-al-punto-de-origen?-test
  (testing "Pruebas unitarias problema 2"
    (is (true? (problema2/regresan-al-punto-de-origen?)))
    (is (true? (problema2/regresan-al-punto-de-origen? [])))
    (is (true? (problema2/regresan-al-punto-de-origen? "")))
    (is (true? (problema2/regresan-al-punto-de-origen? [] "" (list))))
    (is (true? (problema2/regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) "")))
    (is (true? (problema2/regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v))))

    (is (false? (problema2/regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])))
    (is (false? (problema2/regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")))
    (is (false? (problema2/regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])))))

(deftest regreso-al-punto-de-origen-test
  (testing "Pruebas problema 3"
    (is (= () (problema3/regreso-al-punto-de-origen "")))
    (is (= () (problema3/regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))))
    (is (= (seq [\< \< \<]) (problema3/regreso-al-punto-de-origen ">>>")))
    (is (= (seq [\< \< \^ \^ \^ \>]) (problema3/regreso-al-punto-de-origen [\< \v \v \v \> \>])))))

(deftest mismo-punto-final?-test
  (testing "Pruebas problema 4"
    (is (true? (problema4/mismo-punto-final? "" [])))
    (is (true? (problema4/mismo-punto-final? "^^^" "<^^^>")))
    (is (true? (problema4/mismo-punto-final? [\< \< \< \>] (list \< \<))))
    (is (true? (problema4/mismo-punto-final? (list \< \v \>) (list \> \v \<))))

    (is (false? (problema4/mismo-punto-final? "" "<")))
    (is (false? (problema4/mismo-punto-final? [\> \>] "<>")))
    (is (false? (problema4/mismo-punto-final? [\> \> \>] [\> \> \> \>])))
    (is (false? (problema4/mismo-punto-final? (list) (list \^))))))

