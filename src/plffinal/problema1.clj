(ns plffinal.problema1)
;; 1 Dada una secuencia de sentidos indicar si se regresa al punto de origen
(defn vali
  [x]
  (cond
    (nil? x) 0
    (int? x) x))

(defn cantidad
  [x y]
  (vali ((frequencies x) y)))

(defn regresa-al-punto-de-origen?
  [x]
  (and (zero? (- (cantidad x \^) (cantidad x \v)))
       (zero? (- (cantidad x \>) (cantidad x \<)))))


(regresa-al-punto-de-origen? "")
(regresa-al-punto-de-origen? [])
(regresa-al-punto-de-origen? (list))
(regresa-al-punto-de-origen? "><")
(regresa-al-punto-de-origen? (list \> \<))
(regresa-al-punto-de-origen? "v^")
(regresa-al-punto-de-origen? [\v \^])
(regresa-al-punto-de-origen? "^>v<")
(regresa-al-punto-de-origen? (list \^ \> \v \<))
(regresa-al-punto-de-origen? "<<vv>>^^")
(regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])

(regresa-al-punto-de-origen? ">")
(regresa-al-punto-de-origen? (list \>))
(regresa-al-punto-de-origen? "<^")
(regresa-al-punto-de-origen? [\< \^])
(regresa-al-punto-de-origen? ">>><<")
(regresa-al-punto-de-origen? (list \> \> \> \< \<))
(regresa-al-punto-de-origen? [\v \v \^ \^ \^])
