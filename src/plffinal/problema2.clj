(ns plffinal.problema2)

;; 2 Dada n secuencias de sentidos indicar si todas regresan a su propio punto de origen

(defn vali
  [x]
  (cond
    (nil? x) 0
    (int? x) x))

(defn cantidad
  [x y]
  (vali ((frequencies x) y)))

(defn regresa
  [x]
  (and (zero? (- (cantidad x \^) (cantidad x \v)))
       (zero? (- (cantidad x \>) (cantidad x \<)))))

(defn regresan-al-punto-de-origen?
  [& x]
  (cond
    (empty? x) true
    :else (every? true? (map regresa x))))

(regresan-al-punto-de-origen?)
(regresan-al-punto-de-origen? [])
(regresan-al-punto-de-origen? "")
(regresan-al-punto-de-origen? [] "" (list))
(regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) "")
(regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v))

(regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])
(regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")
(regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])