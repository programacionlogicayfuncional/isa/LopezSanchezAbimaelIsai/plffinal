(ns plffinal.problema3)

;; 3 Dada una secuencia de sentidos, regresar la secuencia de sentidos que permita regresar al punto de origen en sentido contrario
(defn vali
  [x]
  (cond
    (nil? x) 0
    (int? x) x))

(defn cantidad
  [x y]
  (vali ((frequencies x) y)))

(defn signox
  [x]
  (cond
    (neg? x) \v
    (pos? x) \^
    (zero? x) \space))

(defn signoy
  [x]
  (cond
    (neg? x) \<
    (pos? x) \>
    (zero? x) \space))

(defn resta
  [x y z]
  (-
   (cantidad x y)
   (cantidad x z)))

(defn regreso-al-punto-de-origen
  [x]
  (concat
   (repeat (cond
             (neg? (resta x \v \^)) (* -1 (resta x \v \^))
             (pos? (resta x \v \^)) (resta  x \v \^)
             :else 0)
           (signox (resta x \v \^)))
   (repeat (cond
             (neg? (resta x \< \>)) (* -1 (resta x \< \>))
             (pos? (resta x \< \>)) (resta x \< \>)
             :else 0)
           (signoy (resta x \< \>)))))

(regreso-al-punto-de-origen "")
(regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))
(regreso-al-punto-de-origen ">>>")
(regreso-al-punto-de-origen [\< \v \v \v \> \>])