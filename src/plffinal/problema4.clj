(ns plffinal.problema4)
;; 4 Dadas 2 secuencias de sentidos, las cuales parten del mismo punto de origen, indicar si ambas terminan en el mismo punto final

(defn vali
  [x]
  (cond
    (nil? x) 0
    (int? x) x))

(defn cantidad
  [x y]
  (vali ((frequencies x) y)))

(defn resta
  [x y z]
  (-
   (cantidad x y)
   (cantidad x z)))

(defn mismo-punto-final?
  [x y]
  (and (=
        (resta x \v \^)
        (resta y \v \^))
       (=
        (resta x \< \>)
        (resta y \< \>))))

(mismo-punto-final? "" [])
(mismo-punto-final? "^^^" "<^^^>")
(mismo-punto-final? [\< \< \< \>] (list \< \<))
(mismo-punto-final? (list \< \v \>) (list \> \v \<))

(mismo-punto-final? "" "<")
(mismo-punto-final? [\> \>] "<>")
(mismo-punto-final? [\> \> \>] [\> \> \> \>])
(mismo-punto-final? (list) (list \^))