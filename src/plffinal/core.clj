(ns plffinal.core)
;; 1 Dada una secuencia de sentidos indicar si se regresa al punto de origen
(defn vali
  [x]
  (cond
    (nil? x) 0
    (int? x) x))

(defn cantidad
  [x y]
  (vali ((frequencies x) y)))

(defn regresa-al-punto-de-origen?
  [x]
  (and (zero? (- (cantidad x \^) (cantidad x \v)))
       (zero? (- (cantidad x \>) (cantidad x \<)))))

;; 2 Dada n secuencias de sentidos indicar si todas regresan a su propio punto de origen

(defn regresa
  [x]
  (and (zero? (- (cantidad x \^) (cantidad x \v)))
       (zero? (- (cantidad x \>) (cantidad x \<)))))

(defn regresan-al-punto-de-origen?
  [& x]
  (cond
    (empty? x) true
    :else (every? true? (map regresa x))))

;; 3 Dada una secuencia de sentidos, regresar la secuencia de sentidos que permita regresar al punto de origen en sentido contrario

(defn signox
  [x]
  (cond
    (neg? x) \v
    (pos? x) \^
    (zero? x) \space))

(defn signoy
  [x]
  (cond
    (neg? x) \<
    (pos? x) \>
    (zero? x) \space))

(defn resta
  [x y z]
  (-
   (vali ((frequencies x) y))
   (vali ((frequencies x) z))))

(defn regreso-al-punto-de-origen
  [x]
  (concat
   (repeat (cond
             (neg? (resta x \v \^)) (* -1 (resta x \v \^))
             (pos? (resta x \v \^)) (resta  x \v \^)
             :else 0)
           (signox (resta x \v \^)))
   (repeat (cond
             (neg? (resta x \< \>)) (* -1 (resta x \< \>))
             (pos? (resta x \< \>)) (resta x \< \>)
             :else 0)
           (signoy (resta x \< \>)))))

;; 4 Dadas 2 secuencias de sentidos, las cuales parten del mismo punto de origen, indicar si ambas terminan en el mismo punto final


(defn mismo-punto-final?
  [x y]
  (and (=
        (resta x \v \^)
        (resta y \v \^))
       (=
        (resta x \< \>)
        (resta y \< \>))))

;; 1
;; TRUE


(regresa-al-punto-de-origen? "")
(regresa-al-punto-de-origen? "><")
(regresa-al-punto-de-origen? (list \> \<))
(regresa-al-punto-de-origen? "v^")
(regresa-al-punto-de-origen? [\v \^])
(regresa-al-punto-de-origen? "^>v<")
(regresa-al-punto-de-origen? (list \^ \> \v \<))
(regresa-al-punto-de-origen? "<<vv>>^^")
(regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])
;; FALSE
(regresa-al-punto-de-origen? ">")
(regresa-al-punto-de-origen? (list \>))
(regresa-al-punto-de-origen? "<^")
(regresa-al-punto-de-origen? [\< \^])
(regresa-al-punto-de-origen? ">>><<")
(regresa-al-punto-de-origen? (list \> \> \> \< \<))
(regresa-al-punto-de-origen? [\v \v \^ \^ \^])

;; 2
;; TRUE
(regresan-al-punto-de-origen?)
(regresan-al-punto-de-origen? [])
(regresan-al-punto-de-origen? "")
(regresan-al-punto-de-origen? [] "" (list))
(regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) "")
(regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v))
;; FALSE
(regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])
(regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")
(regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])

;; 3
(regreso-al-punto-de-origen "")
(regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))

(regreso-al-punto-de-origen ">>>")
(regreso-al-punto-de-origen [\< \v \v \v \> \>])
;; 4
(mismo-punto-final? "" [])
(mismo-punto-final? "^^^" "<^^^>")
(mismo-punto-final? [\< \< \< \>] (list \< \<))
(mismo-punto-final? (list \< \v \>) (list \> \v \<))

(mismo-punto-final? "" "<")
(mismo-punto-final? [\> \>] "<>")
(mismo-punto-final? [\> \> \>] [\> \> \> \>])
(mismo-punto-final? (list) (list \^))
